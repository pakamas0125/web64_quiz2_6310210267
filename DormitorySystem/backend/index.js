const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const mysql = require('mysql');
const connection = mysql.createConnection({
    host : 'localhost',
    user : 'Dormbooking',
    password : '6310210267',
    database : 'DormbookingSystem'
})

connection.connect();
const express = require('express');
const req = require('express/lib/request');
const app = express()
const port = 4000

const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET =process.env.TOKEN_SECRET

const jwt = require('jsonwebtoken')

function authenticatingToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if(err){ return res.sendStatus(403)}
        else {
            req.user = user
            next()
        }
    })
}

app.post("/add_dormitory", (req, res) => {
    let dormitory_name = req.query.dormitory_name
    let dormitory_location = req.query.dormitory_location

    let query = `INSERT INTO Addressdormitory
                    (DormitoryName, Dormitorylocation)
                    VALUES ('${dormitory_name}','${dormitory_location}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error inserting data into db"
                    })
        }else{
            res.json({
                "status" : "200",
                "message" : "Adding dormitory succesful"
              })
        }
    })
})

app.get("/list_dormitory",authenticatingToken,(req, res) => {
    let query = "SELECT * from Addressdormitory"
    connection.query( query, (err, rows) => {
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error querying from running db"
                    })
        }else{
            res.json(rows)
        }
    })
})

app.post("/update_dormitory",authenticatingToken, (req, res) => {
    
    let dormitory_id = req.query.dormitory_id
    let dormitory_name = req.query.dormitory_name
    let dormitory_location = req.query.dormitory_location

    let query = `UPDATE Addressdormitory SET
                    DormitoryName='${dormitory_name}',
                    Dormitorylocation='${dormitory_location}'
                    WHERE DormitoryID=${dormitory_id}`
    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error updating record"
                    })
        }else{
            res.json({
                "status" : "200",
                "message" : "Updating dormitory succesful"
              })
        }
    })
})

app.post("/delete_dormitory", (req, res) => {
    
    let dormitory_id = req.query.dormitory_id

    let query = `DELETE FROM Addressdormitory 
                    WHERE DormitoryID=${dormitory_id}`
    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error deleting record"
                    })
        }else{
            res.json({
                "status" : "200",
                "message" : "Deleting record succesful"
              })
        }
    })
})

app.post("/registerTenant", (req, res) => {
    
    let nameTenant = req.query.nameTenant
    let surnameTenant = req.query.surnameTenant
    let usernameTenant = req.query.usernameTenant
    let passwordTenant= req.query.passwordTenant
    
    bcrypt.hash(passwordTenant, SALT_ROUNDS, (err,hash) => {

        let query = `INSERT INTO Tenant
                    (TenantName, TenantSurname, Username, Password)
                    VALUES ('${nameTenant}','${surnameTenant}','${usernameTenant}','${hash}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error inserting data into db"
                    })
        }else{
            res.json({
                "status" : "200",
                "message" : "Adding new user succesful"
              })
        }
    })
  })

})

app.post("/login", (req, res) =>{
    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM Tenant WHERE Username = '${username}'`
    connection.query(query, (err, rows) => {
        if (err) {
            console.log (err)
            res.json({
                         "status" : "400",
                         "message" : "Error querying from running db"
                     })
        }else{
            let db_password = rows[0].Password
            bcrypt.compare(user_password, db_password, (err, result) => {
                if (result) {
                 let payload = {
                     "username" : rows[0].Username,
                     "user_id" : rows[0].RoomID,
                 }
                 console.log(payload)
                 console.log(TOKEN_SECRET)
                 let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn : '1d'})
                 res.send(token)
                }else { res.send("Invalid username / password") }
            })
        }
    })
})


app.listen(port, () => {
    console.log( `Now starting DormitorySystem Backend ${port}`)
})





/*
query = "SELECT * from Tenant";
connection.query( query, (err, rows) => {
    if(err){
        console.log(err);
    }else{
        console.log(rows);
    }
});

connection.end() */
